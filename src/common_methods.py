#!/usr/bin/python
# -*- coding: utf-8 -*-
# 25/06/2018
# N. Villefranque
# Modifs :
## 02/2019 | Arthur Garreau | adapted to Python 3.

import netCDF4 as ncdf
import numpy   as np
import scipy as sp
from collections import OrderedDict

def delete_smaller_than(mask,obj,minval):
  sizes = sp.ndimage.sum(mask,obj,np.unique(obj[obj!=0]))  
  del_sizes = sizes < minval
  del_cells = np.unique(obj[obj!=0])[del_sizes]
  ss        = obj.shape
  objf      = obj.flatten()
  ind       = np.in1d(objf,del_cells)
  objf[ind] = 0
  obj       = objf.reshape(ss)
  return (obj)

def removebounds(tmp):
   if len(tmp.shape) == 1.:
     tmp = tmp[1:-1]
   elif len(tmp.shape) == 3.:
     tmp = tmp[1:-1,1:-1,1:-1]
   else:
     print ('Problem with removebounds')
     exit(1)
   return (tmp)

def addbounds(tmp):
   ss   = [ij+2 for ij in tmp.shape]
   tmp2 = np.zeros(ss)*np.nan
   if len(tmp.shape) == 1.:
     tmp2[1:-1] = tmp
   elif len(tmp.shape) == 3.:
     tmp2[1:-1,1:-1,1:-1] = tmp
   else:
     print ('Problem with addbounds')
     exit(1)
   return (tmp2)


def check_name_in_ncdf(myname,ncvars,ncdfFile,overwrite=False) :
    name = myname
    while(myname in ncvars) :
        print ('\t', myname, 'already exists in', ncdfFile)
        if not overwrite:
            name = input('\toverwrite (o), rename (type new name), exit (e) ?\n')
        else:
          name = 'o'
        
        if name=='o' : break
        elif name=='e' : exit()
        else : myname = name ; print("OK to rename as",myname)
    return (name)

def read(ncdfFile, listVarNames, name, dimMask=(), rmbounds=False,
        simple_prec=True, overwrite=False) :
  dset = ncdf.Dataset(ncdfFile, 'r')
  dictVars = OrderedDict()
  if isinstance(listVarNames, type(None)) : raise TypeError('listVarNames is empty')
  if isinstance(listVarNames, str) : listVarNames = list(listVarNames)
  if (not overwrite) and (name in dset.variables):
      print('Variable %s already in file %s. '%(name, ncdfFile))
      print('Set overwrite=True to overwrite')
      return None,None
  for i,n in enumerate(listVarNames) :
    if not n in dset.variables: raise TypeError('Variable %s not in file %s'%(n, ncdfFile))
    v = dset.variables[n]
    if i==0:
      if len(dimMask)==0: dims = v.dimensions
      else : dims=tuple([v.dimensions[d] for d in dimMask])
    if rmbounds:
      v = removebounds(v)
    if simple_prec : dictVars[n]=v[:].astype("float32").reshape(v.shape)
    else : dictVars[n]=v[:].reshape(v.shape)
  dset.close()
  return (dictVars,dims)

def stats(tab):
    return ([np.mean(tab),np.min(tab),np.max(tab),np.std(tab),np.median(tab),np.percentile(tab,5),np.percentile(tab,95),np.sum(tab)])

def addUserVars(dictVars,dictFuncUserVars):
    for n in dictFuncUserVars:
        dictVars[n]=dictFuncUserVars[n](dictVars)
    return (dictVars)
