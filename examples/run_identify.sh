cas=FIRE
sens=(L25.6)
suffix=(012)

name=(V0301)
#if [ "$cas" = "ASTEX" ] ; then
#name=(STD01 STD03 STD05 STD07 STD09 STD11 STD13 STD15 STD17 STD19 STD21)
#fi
for ss in "${sens[@]}"
do

# Your path
path="/cnrm/tropics/user/brientf/MESONH/$cas/$ss/"

for nn in "${name[@]}"
do
for ij in "${suffix[@]}"
do

echo 'a'
filename=$path/$ss.1.$nn.$ij.nc4

echo $filename

typs=(downdraft)
# Your variable for the typ above
tracer=SVT006
for typ in "${typs[@]}"
do
echo 'down'
python identify_downdrafts_and_updrafts.py $typ $tracer $filename
done

typs=(updraft)
# Your variable for the typ above
tracer=SVT004
for typ in "${typs[@]}"
do
echo 'up'
python identify_downdrafts_and_updrafts.py $typ $tracer $filename
done

done
done
done
