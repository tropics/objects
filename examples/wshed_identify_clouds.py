#!/usr/bin/python
# -*- coding: latin-1 -*-
# test identification_methods.py : clouds
# 13/06/2018
# N. Villefranque

import os,sys
sys.path.append('../src/')
from identification_methods import identify
import numpy as np
import matplotlib.pyplot as plt

ncdfFile = "../../data/CERK4.1.ARMCu.008.nc"
ncdfFile = "/cnrm/tropics/user/villefranquen/CAS_LES/SIMULATIONS/BOMEX_LES/RDFRC/RDFRC.1.BOMEX.ALL.nc4"

listVarNames = ['RCT',"WT"] # list of relevant variables.

def cloudMask(dictVars) :
    rc = dictVars['RCT']
    mask=rc*0.   # same shape as RCT
    mask[rc>0]=1 # cell is in cloud if liquid water mixing ratio is positive
    return mask

def coreMask(dictVars) :
    rc = dictVars['RCT']
    wt = dictVars["WT"]
    mask=wt*0.
    mask[(wt>0)&(rc>1e-6)] = 1 # core cloud
    return -rc, mask

# To read more info on the function identify, uncomment the following line
#help(identify)
objects, tmp = identify(ncdfFile, listVarNames, cloudMask, funcWatershed=coreMask, name="wshed_obj",delete=100,rename=True,deleteCores=32)
