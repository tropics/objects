#!/usr/bin/python
# -*- coding: latin-1 -*-
# test identification_methods.py : clouds
# 13/06/2018
# N. Villefranque

import os,sys
sys.path.append('../src/')
from identification_methods import identify
import numpy as np
import matplotlib.pyplot as plt
from skimage.feature import peak_local_max

ncdfFile = "/cnrm/tropics/user/villefranquen/CAS_LES/SIMULATIONS/BOMEX_LES/RDFRC/RDFRC.1.BOMEX.ALL.nc4"
ncdfFile = "../../data/CERK4.1.ARMCu.008.nc"
listVarNames = ['RCT','THT','PABST','WT',"VLEV"]
ra = 287.
cp = 1004.
ra_over_cp = ra/cp
rho_w = 1000.
r_eff = 10.0e-9
def ke(dictVars) :
  rc = dictVars['RCT']; th = dictVars['THT']; pr = dictVars['PABST']
  p0 = pr[:,0,:,:]
  den = ra*th*p0**(-ra_over_cp)
  rho = pr**(1.-ra_over_cp)/den
  ke = 3./2.*rc*rho/(r_eff*rho_w)
  return ke

def od(dictVars):
    rc = dictVars['RCT']
    zz = dictVars["VLEV"][:,0,0]
    dz = zz[1:]-zz[:-1]
    kem = ke(dictVars)
    od = np.sum(kem[:,:-1,:,:]*dz[None,:,None,None],axis=1)
    return od

def cloudMask(dictVars) :
    odm = od(dictVars)
    mask=odm*0.   # same shape as od
    mask[odm>0]=1 # cell is in cloud if optical depth is positive
    return mask

def coreMask(dictVars) :
    odm = od(dictVars)
    mask = odm*0.
    wt = np.max(dictVars["WT"],axis=1)
    #mask[wt>np.mean(wt)+2*np.std(wt)] = 1
    mask[odm>np.mean(odm)+2*np.std(odm)] = 1
    return -odm, mask

# To read more info on the function identify, uncomment the following line
#help(identify)
#objects, tmp = identify(ncdfFile, listVarNames, cloudMask,  name="obj_2D",delete=32,dimMask=(0,2,3),rename=True)
objects, tmp = identify(ncdfFile, listVarNames, cloudMask, funcWatershed=coreMask, dimMask=(0,2,3), name="wshed_obj_2D_2sig_del_cy",cyclic=(-1,-2),delete=32,deleteCores=16)
