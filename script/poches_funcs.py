import sys, os
sys.path.append('../src/')
from identification_methods import identify
from characterization_methods import characterize
import numpy as np
import scipy

# list of variables that will be used 
listVarsNames = ['THT', 'RRT', 'UT', 'VT', 'WT', 'RIT', 'RCT',
        'vertical_levels', 'W_E_direction', 'S_N_direction']
listVarsNames = ['THT', 'UT', 'VT',
        'vertical_levels', 'W_E_direction', 'S_N_direction']

# Setup
overwrite=True #False

s_thresholds_skin = ["m0d1", "m0d2", "m0d3", "m0d5", "m0d8"]
s_thresholds_core = ["m0d8", "m1d0", "m1d2"]
s_thresholds_gust = ["m0d1", "m0d2", "m0d3"]
s_fac_std_winds   = ["0d5", "0d8", "1d0"]

#-------------------------------------------------#
#  mask functions that will be passed to identify #
#-------------------------------------------------#

def poche_id_simple(dictVars, threshold):
  theta = dictVars["THT"]
  theta_surf = theta[:,0,:,:]
  theta_surf_ano = theta_surf - np.mean(theta_surf, axis=(-1,-2))[:,None,None]
  mask = theta_surf*0.
  mask[theta_surf_ano < threshold] = 1
  return mask

def poche_id_watershed(dictVars, threshold):
  theta = dictVars["THT"]
  theta_surf = theta[:,0,:,:]
  theta_surf_ano = theta_surf - np.mean(theta_surf, axis=(-1,-2))[:,None,None]
  mask = theta_surf*0.
  mask[theta_surf_ano < threshold] = 1
  return theta_surf_ano, mask

def poche_id_watershed_gust(dictVars, threshold_th, fac_std_wind):
  theta = dictVars["THT"]
  theta_surf = theta[:,0,:,:]
  theta_surf_ano = theta_surf - np.mean(theta_surf, axis=(-1,-2))[:,None,None]
  wind = np.sqrt(dictVars["UT"][:,0,:,:]**2+dictVars["VT"][:,0,:,:]**2)
  wind_smooth = scipy.ndimage.uniform_filter(wind, size=(1,16,16), mode='wrap')
  wind_surf_ano = wind_smooth - np.mean(wind, axis=(-1,-2))[:,None,None]
  wind_surf_std = np.std(wind, axis=(-1,-2))[:,None,None]
  mask = wind*0.
  mask[(wind_surf_ano > wind_surf_std*fac_std_wind) & (theta_surf_ano < threshold_th)] = 1
  return theta_surf_ano, mask

def get_move(dictVars, dt):
  wind_u = dictVars["UT"]
  wind_v = dictVars["VT"]
  wind_u_surf_mean = np.mean(wind_u[:,0,:,:], axis=(-1,-2))
  wind_v_surf_mean = np.mean(wind_v[:,0,:,:], axis=(-1,-2))
  dx = (dictVars["W_E_direction"][1] - dictVars["W_E_direction"][0])*1000
  dy = (dictVars["S_N_direction"][1] - dictVars["S_N_direction"][0])*1000
  move_x = wind_u_surf_mean * dt / dx
  move_y = wind_v_surf_mean * dt / dy
  return move_x, move_y

def poche_treat_unremove_wind(dictVars, mask, dt):
  move_x, move_y = get_move(dictVars, dt)
  for i in range(1, mask.shape[0]):
      mask[i,:,:] = np.roll(mask[i,:,:], int(np.sum(move_x[:i])), axis=-1)
      mask[i,:,:] = np.roll(mask[i,:,:], int(np.sum(move_y[:i])), axis=-2)
  return mask

def poche_id_simple_remove_wind(dictVars, threshold, dt):
  plot=0
  theta = dictVars["THT"]
  theta_surf = theta[:,0,:,:]
  theta_surf_ano = theta_surf - np.mean(theta_surf, axis=(-1,-2))[:,None,None]
  mask = theta_surf*0.
  mask[theta_surf_ano < threshold] = 1
  move_x, move_y = get_move(dictVars, dt)
  for i in range(1, mask.shape[0]):
      mask[i,:,:] = np.roll(mask[i,:,:], int(-np.sum(move_x[:i])), axis=-1)
      mask[i,:,:] = np.roll(mask[i,:,:], int(-np.sum(move_y[:i])), axis=-2)
  return mask

def poche_id_watershed_remove_wind(dictVars, threshold, dt):
  theta = dictVars["THT"]
  theta_surf = theta[:,0,:,:]
  theta_surf_ano = theta_surf - np.mean(theta_surf, axis=(-1,-2))[:,None,None]
  mask = theta_surf*0.
  mask[theta_surf_ano < threshold] = 1
  dist = theta_surf_ano
  move_x, move_y = get_move(dictVars, dt)
  for i in range(1, mask.shape[0]):
      mask[i,:,:] = np.roll(mask[i,:,:], int(-np.sum(move_x[:i])), axis=-1)
      mask[i,:,:] = np.roll(mask[i,:,:], int(-np.sum(move_y[:i])), axis=-2)
      dist[i,:,:] = np.roll(dist[i,:,:], int(-np.sum(move_x[:i])), axis=-1)
      dist[i,:,:] = np.roll(dist[i,:,:], int(-np.sum(move_y[:i])), axis=-2)
  return theta_surf_ano, mask

#-------------------------------------------------#
#     master functions that call identfiy         # 
#-------------------------------------------------#

def str2float(str):
  # replace "m" with "-" and "d" with "." and convert to float
  if "m" in str: return float(".".join("-".join(str.split("m")).split("d")))
  # replace "d" with "." and convert to float
  else: return float(".".join(str.split("d")))

def func_id_simple(mask, name, file, args=None):
  if not os.path.isfile(file): print("%s does not exist"%file); return
  for s_threshold in s_thresholds_skin:
    this_name = name+"_threshold%s"%s_threshold
    threshold = str2float(s_threshold)
    print(threshold)
    identify(file, listVarsNames, mask[0] if type(mask) is list else mask, 
            argsCalcMask=threshold if args is None else (threshold, args),
            funcTreatMask = mask[1] if type(mask) is list else None,
            argsTreatMask = args if type(mask) is list else (),
            dimMask=(0,2,3), overwrite=overwrite, name=this_name)

def func_id_watershed(masks, name, file, args=None):
  if not os.path.isfile(file): print("%s does not exist"%file); return
  for s_threshold1 in s_thresholds_skin: 
    for s_threshold2 in s_thresholds_core:
      this_name = name+"_threshold1%s_threshold2%s"%(s_threshold1,s_threshold2)
      threshold1 = str2float(s_threshold1)
      threshold2 = str2float(s_threshold2)
      identify(file, listVarsNames, masks[0], funcWatershed=masks[1],
            argsCalcMask=threshold1 if args is None else (threshold1, args),
            argsWatershed=threshold2 if args is None else (threshold2, args),
            funcTreatMask = masks[2] if len(masks)>2 else None,
            argsTreatMask = args if len(masks)>2 else (),
            funcTreatCore = masks[3] if len(masks)>2 else None,
            argsTreatCore = args if len(masks)>2 else (),
            dimMask=(0,2,3), rename=True, overwrite=overwrite,
            name=this_name)

def func_id_watershed_gust(masks, name, file, args=None):
  if not os.path.isfile(file): print("%s does not exist"%file); return
  for s_threshold1 in s_thresholds_skin: 
    for s_threshold2 in s_thresholds_gust:
      for s_fac_std_wind in s_fac_std_winds:
        this_name = name+"_threshold1%s_gust_fac%s_and_threshold2%s"%(
                s_threshold1, s_fac_std_wind, s_threshold2)
        threshold1 = str2float(s_threshold1)
        threshold2 = str2float(s_threshold2)
        fac_std_wd = str2float(s_fac_std_wind)
        identify(file, listVarsNames, masks[0], funcWatershed=masks[1],
              argsCalcMask=threshold1 if args is None else (threshold1, args),
              argsWatershed=(threshold2,fac_std_wd) if args is None else
              (threshold2,fac_std_wd,args),
              funcTreatMask = masks[2] if len(masks)>2 else None,
              argsTreatMask = args if len(masks)>2 else (),
              funcTreatCore = masks[3] if len(masks)>2 else None,
              argsTreatCore = args if len(masks)>2 else (),
              dimMask=(0,2,3), rename=True, overwrite=overwrite,
              name=this_name)

#-------------------------------------------------#
#     master functions that call characterize     # 
#-------------------------------------------------#

def extract_first_level(field):
  if len(field.shape)==4:
    return field[:,0,:,:]
  else: return field

def func_charac_simple(name, file):
  if not os.path.isfile(file): print("%s does not exist"%file); return
  for s_threshold in s_thresholds_skin:
    this_name = name+"_threshold%s"%s_threshold
    out_file = file.split(".nc")[0]+"_charac_%s.nc"%this_name
    characterize(file, listVarsNames, funcExtractField=extract_first_level,
            name=this_name, ncdfOut=out_file)

def func_charac_watershed(name, file):
  if not os.path.isfile(file): print("%s does not exist"%file); return
  for s_threshold1 in s_thresholds_skin: 
    for s_threshold2 in s_thresholds_core:
      this_name = name+"_threshold1%s_threshold2%s"%(s_threshold1,s_threshold2)
      out_file = file.split(".nc")[0]+"_charac_%s.nc"%this_name
      characterize(file, listVarsNames, funcExtractField=extract_first_level,
              name=this_name, ncdfOut=out_file)

def func_charac_watershed_gust(name, file):
  if not os.path.isfile(file): print("%s does not exist"%file); return
  for s_threshold1 in s_thresholds_skin: 
    for s_threshold2 in s_thresholds_gust:
      for s_fac_std_wind in s_fac_std_winds:
        this_name = name+"_threshold1%s_gust_fac%s_and_threshold2%s"%(
                s_threshold1, s_fac_std_wind, s_threshold2)
        out_file = file.split(".nc")[0]+"_charac_%s.nc"%this_name
        characterize(file, listVarsNames, funcExtractField=extract_first_level,
                name=this_name, ncdfOut=out_file)
