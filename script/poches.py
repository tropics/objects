# 31/01/2023
# Najda Villefranque

# Identify cold pools

from poches_funcs import *
import os,sys

dpath="/home/nvillefranque/work/data/LES/RCE/"

times = [ 180  , 360  , 540  , 720  , 900  , 1080 , 1260 , 1440 , 1620 , 1800 ]
list_dt = times[:-1]

# each file corresponds to a different time step
def file_t(t): return dpath+"formatted_RCE3D_MNH_DAY12_%i.nc"%t
files_t = [file_t(time) for time in times]

# all time steps have been concatenated to this file
def file_dt(dt): return dpath+"formatted_RCE3D_MNH_DAY12_concat_dt%i.nc"%dt
files_dt = [file_dt(dt) for dt in list_dt]

def select_test(itest):
  if   itest == 1:
    test = 1
    desc = """Identifying the cold pools tstep by tstep with a simple criteria"""
    mask = poche_id_simple
    name = "poches_test%i"%test
    def func(mask, name):
      for time, file in zip(times, files_t) :
        func_id_simple(mask, name, file)
        func_charac_simple(name, file)
  
  elif itest == 2:
    test = 2
    desc = """Identifying the cold pools one by one with a watershed method"""
    mask = [poche_id_simple, poche_id_watershed]
    name = "poches_test%i"%test
    def func(mask, name):
      for time, file in zip(times, files_t) :
        func_id_watershed(mask, name, file)
        func_charac_watershed(name, file)

  elif itest == 3:
    test = 3
    desc = """Identifying the cold pools all tsteps at once, with a simple
    criteria"""
    mask = poche_id_simple
    name = "poches_test%i"%test
    def func(mask, name):
      for dt,file in zip(list_dt,files_dt):
        func_id_simple(mask, name, file)
        func_charac_simple(name, file)

  elif itest == 4:
    test = 4
    desc = """Identifying the cold pools all tsteps at once, with watershed"""
    mask = [poche_id_simple, poche_id_watershed]
    name = "poches_test%i"%test
    def func(mask, name):
      for dt,file in zip(list_dt,files_dt):
        func_id_watershed(mask, name, file)
        func_charac_watershed(name, file)

  elif itest == 5:
    test = 5
    desc = """Identifying the cold pools all tsteps at once, with simple
    criteria, removing wind"""
    mask = [poche_id_simple_remove_wind, poche_treat_unremove_wind]
    name = "poches_test%i"%test
    def func(mask, name):
      for dt,file in zip(list_dt,files_dt):
        func_id_simple(mask, name, file, args=dt)
        func_charac_simple(name, file)

  elif itest == 6:
    test = 6
    desc = """Identifying the cold pools all tsteps at once, with watershed,
    removing wind"""
    mask = [poche_id_simple_remove_wind, poche_id_watershed_remove_wind,
            poche_treat_unremove_wind, poche_treat_unremove_wind]
    name = "poches_test%i"%test
    def func(mask, name):
      for dt,file in zip(list_dt,files_dt):
        func_id_watershed(mask, name, file, args=dt)
        func_charac_watershed(name, file)

  elif itest == 7:
    test = 7
    desc = """Identifying the cold pools all tsteps at once, with watershed,
    core = gust, skin = ano_theta"""
    mask = [poche_id_simple, poche_id_watershed_gust]
    name = "poches_test%i"%test
    def func(mask, name):
      for dt,file in zip(list_dt,files_dt):
        func_id_watershed_gust(mask, name, file)
        func_charac_watershed_gust(name, file)

  else:
    print("Could not find test %i"%itest)
    return None, None, None, None, None

  return test, desc, mask, name, func

def run_test(itest, verbose):
  jtest, desc, mask, name, func = select_test(itest)
  if jtest is None: return
  print("--"*35)
  print("Test %i: %s"%(itest,desc))
  print("--"*35)
  if not verbose:
    func(mask, name)
    print("--"*35)
  return

if __name__=="__main__":
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument("-i", help="itest [i1,i2,...]", required=True)
  parser.add_argument("-v", help="verbose", action="store_true") 
  args = parser.parse_args()
  itests = [int(i) for i in (args.i).split(",")]
  verbose = args.v
  for itest in itests: run_test(itest, verbose)
