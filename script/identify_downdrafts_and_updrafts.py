#!/usr/bin/python
# -*- coding: latin-1 -*-
# test identification_methods.py : updrafts
# 13/06/2018
# N. Villefranque
# modif F Brient
# call : python identify_downdrafts_and_updrafts.py downdraft SVT006 /cnrm/tropics/user/brientf/MESONH/FIRE/L25.6/L25.6.1.V0301.003.nc4

import sys
sys.path.append('../src/')
from identification_methods import identify
import numpy as np
#import pylab as plt


# Anomalies 3D relative to 2D average
def anom(data):
    sz    = data.shape
    NS    = len(sz)
    mean  = np.mean(data,axis=(NS-2,NS-1))
    if len(sz)==4:
      mean  = np.tile(mean[:,:,None,None],(1,1,sz[-2],sz[-1]))
    else:
      mean  = np.tile(mean[:,None,None],(1,sz[1],sz[2]))
    data  = data-mean # anomalies
    return data

# Find nearrest point 
def near(array,value):
  idx=(abs(array-value)).argmin()
  return idx


# user function to remove some parts of the domain
# by default used to remove anomalies above the cloud-top
def suppzdata(thrs,zmin,zmax):
    if zmax is not None:
      hh   = np.ones(thrs.shape)*max(thrs)*3 # never happen
      hh[zmin:zmax]=thrs[zmin:zmax]
      thrs = hh
    return thrs


# Find minimum threshold for tracers vertical evolution.
def findsigmaming(z,zdemi,data,rang):
    # z     is the altitude in meters (middle of layers)
    # zdemi is the altitude in meters (interlayers)
    # data  is the 3D tracer anomaly
    # rang  are indexes over which sigma are calculated
    # Based on Couvreux et. al (10)

    # Output : sigma and sigma_min

    resSigma    = np.zeros(len(zdemi))
    resSigmamin = np.zeros(len(zdemi))
    
    dz          = z[np.array(rang)+1]-z[rang]
    data        = np.squeeze(data)
    sigma       = np.std(data[rang,:,:],axis=(1,2))
    integral    = np.cumsum(sigma*dz)
    sigmamin    = 0.05/zdemi[rang]*integral

    resSigma[rang]    = sigma
    resSigmamin[rang] = sigmamin

    return resSigma,resSigmamin


# Find cloud base and cloud top
# Define as the first layer where RCT (ql) > epsilon
def findbasetop(data,epsilon):
    cloudbase = 0; cloudtop = 0;
    for ij in range(len(data)):
      if cloudbase == 0 and data[ij] > epsilon:
         cloudbase = ij
      if cloudbase != 0  and cloudtop == 0  and data[ij] < epsilon:
         cloudtop  = ij
    #print 'base,top : ',cloudbase,cloudtop,data*1000.
    return cloudbase,cloudtop



# Find the cloud base, the cloud top and the middle of cloud
# Save 3D field of cloud base and top, and the domain averaged of this field
def cloudinfo(rct,epsilon):
    # First computation : Use the mean 
    meanql             = np.mean(np.mean(rct,axis=2),axis=1)
    cloudbase,cloudtop = findbasetop(meanql,epsilon)

    # Second computation : Each vertical layer
    ny,nx      = rct.shape[1],rct.shape[2]
    cloudbase0 = np.zeros((ny,nx))
    cloudtop0  = np.zeros((ny,nx))
    for ix in range(nx):
      for iy in range(ny):
         data = rct[:,iy,ix]
         cloudbase0[iy,ix],cloudtop0[iy,ix] = findbasetop(data,epsilon)     

    # Real values
    cloudbase2 = np.nanmean(cloudbase0[:])
    cloudtop2  = np.nanmean(cloudtop0[:])

    # Make integer
    cloudbase   = int(cloudbase2)
    cloudtop    = int(cloudtop2)+1
    cloudmiddle = int(round((cloudbase+cloudtop)/2))
    
    print('Compute final :')
    print('cloudbase : ij='+str(cloudbase))
    print('cloudtop  : ij='+str(cloudtop))
    return cloudbase,cloudmiddle,cloudtop,cloudbase0,cloudtop0


# Select objects based on tracers
# Methodology based on Couvreux et. al (10, BLM)
# Methodology used in Brient et. al (19, GRL)
def selSVT(dictVars,mm,nameobj) :
    
    #Find if down or up (used only for double condition in SVT002)
    name = nameobj.split('_')[0] 

    # Find SVT and W in dictVars
    Wname,SVT,RCT = None,None,None
    for k in dictVars :
      if "SVT" in k or "RVT" in k : SVT=k
      if "WT"  in k : Wname=k
      if "RCT" in k : RCT=k
  
    # Conditional sampling
    if SVT is not None :
      data0  = dictVars[SVT]
    else :
      "No SVT var was found!" ; exit(1)
     
    # Additional conditional sampling  
    if Wname is not None :
      W    = dictVars[Wname]
    if RCT   is not None :
      QL   = dictVars[RCT]

    sz    = data0.shape
    NS    = len(sz)

    # Data anomaly and mask
    data  = anom(data0)
    mask  = data*0.
    
    # Vertical levels
    z     = dictVars[nameZ] #nameZ="ZHAT"
    
    # inter-layers
    zdemi = np.array([(z[ij]+z[ij+1])/2.0 for ij in range(len(z)-1)])
    zdemi = np.append(zdemi,z[-1]+(z[-1]-z[-2])/2.)

    # Index wherein lies the maximum of the tracer concentration
    mean   = np.nanmean(data0,axis=(NS-2,NS-1))
    idxsvt = np.argmax(mean)
      
    # Defin zmin
    zmin  = 0
    # If cloud-base identification, define zmin at the altitude 
    # where tracer concentration is maximum
    if ((SVT=='SVT002' or SVT=='SVT005') and name=='updr'):
      zmin   = idxsvt
      
    # Define zmax
    if SVT=='SVT003' or SVT=='SVT006' or \
       ((SVT=='SVT002' or SVT=='SVT005') and name=='down'): 
      zmax  = idxsvt
      rang  = range(zmax,-1,-1) #downward
    elif SVT=='RVT':
      zmax  = len(zdemi)-2
      rang  = range(zmax,zmin,-1) #downward
    else: # default
      zmax  = len(zdemi)-1
      rang  = range(zmin,zmax) #upward

    sigma,sigmamin = findsigmaming(z,zdemi,data,rang)
     
    # Sigma in each layer
    thrs  = mm*np.maximum(sigma,sigmamin)
    
    # Remove sampling above zmax, and below zmin
    thrs  = suppzdata(thrs,zmin,zmax)
    
    # Make 3D table
    if NS==4:
      thrs = np.tile(thrs[None,:,None,None],(sz[0],1,sz[2],sz[3]))
    else:
      thrs = np.tile(thrs[:,None,None],(1,sz[1],sz[2]))

    # Create mask
    if mm<0:
       mask[data<thrs]=1
    else:
       mask[data>thrs]=1

    # If double condition
    # If vertical velocity
    if Wname is not None:
      # remove points that do not satisfy the WT sampling
      thrsW = 0.0
      thrsW = np.ones((W.shape))*thrsW
      if name=='down': # downdraft
        mask[~(W<thrsW)]=0
      elif name=='updr': # updraft
        mask[~(W>thrsW)]=0

    # If liquid water
    if RCT is not None:
      epsilon = 1e-6
      mask[QL>epsilon]=0


    return mask

def downdraftWT(dictVars,thrs) :
    w            = dictVars['WT']
    mask         = w*0.
    mask[w<thrs] = 1 # cell is in updraft if vertical wind < thrs
    return mask

def updraftWT(dictVars,thrs) :
    w            = dictVars['WT']
    mask         = w*0.
    mask[w>thrs] = 1 # cell is in updraft if vertical wind < thrs
    return mask



if len(sys.argv)!=4 and len(sys.argv)!=5 : 
    print("Usage : python",sys.argv[0], "object varsel ncfile")
    exit(1)

obj     = sys.argv[1] #'downdraft'
varsel  = sys.argv[2] #'SVT006'
ncfile  = sys.argv[3]

vmin    = 0
if len(sys.argv)==5:
  vmin    = sys.argv[4] # Name of the file : path + filename

# manage cases with double condition
varsel2 = None
if '+' in varsel:
   vartmp  = varsel.split('+')
   varsel  = vartmp[0]
   varsel2 = vartmp[1:]

# By default: Selected by tracer concentrations
select   = selSVT
# By default: Conditional sampling scale from m=0 to m=2
listThrs = [0,0.5,1,1.5,2] 

if obj == 'downdraft':
  if varsel == 'WT':
    select   = downdraftWT
    listThrs=[0,-0.25,-0.5,-0.75,-1]
  elif varsel == 'RVT':
    listThrs = [-2,-1.5,-1,-0.5,0] 
elif obj == 'updraft':
  if varsel == 'WT':
    select   = updraftWT
    listThrs=[0,0.25,0.5,0.75,1]
  elif varsel == 'RVT':
    listThrs = [0,0.5,1,1.5,2] 
else : 
  print("Wrong object:"+obj+" or wrong var"+varsel) ; exit(1)

nameZ         = 'vertical_levels' #'ZHAT'
listVarsNames = [varsel,nameZ]  # any size. The mask dimensions must be the same as first var in list
nameobj0      = obj[0:4]+"_"+varsel

if varsel2 is not None:
   print(listVarsNames,varsel2,nameobj0)
   listVarsNames += varsel2
   nameobj0      += "_"+''.join(varsel2)

for ik,thrs in enumerate(listThrs):
  nameobj=nameobj0+"_%02i" %(ik)
  if vmin !=0:
    nameobj+="_"+str(vmin)
  print(ncfile)
  
  rename    = True
  rmbounds  = False
  overwrite = True
  unique    = False
  identify(ncfile, listVarsNames,\
           select, argsCalcMask=(thrs,nameobj,),\
           name=nameobj, delete=vmin,\
           rename=rename, rmbounds=rmbounds, overwrite=overwrite, unique=unique)

